﻿-- PostgreSQL

CREATE TABLE authors(
	id serial PRIMARY KEY,
	last_name VARCHAR(64) NOT NULL,
	first_name VARCHAR(64) NOT NULL,
	middle_name VARCHAR(64) NOT NULL
);

CREATE TABLE books(
	id serial PRIMARY KEY,
	name VARCHAR(64) NOT NULL
	genre VARCHAR(64) NOT NULL, -- Better to use enum
	publisher VARCHAR(64) NOT NULL, -- Better to use reference on the table with publishers
	publication_date date NOT NULL,
);

CREATE TABLE author_book(
	author_id int REFERENCES authors(id) ON DELETE CASCADE,
	book_id int REFERENCES books(id) ON DELETE CASCADE,
	PRIMARY KEY (author_id, book_id)
);
