﻿-- Запрос, выводящий авторов, написавших меньше 7 книг.

SELECT A.id FROM authors A
JOIN author_book AB ON(AB.author_id = A.id)
GROUP BY A.id
HAVING count(A.id) < 7