<?php

define('N', 2000000);
$numbers = array_fill(2, N-1, true);

for ($i = 2; $i**2 <= N; $i++) { 
        if ($numbers[$i]) {
                for ($j = $i**2; $j <= N; $j += $i) { 
                        $numbers[$j] = false;
                }
        }
}

$sum = 0;
foreach ($numbers as $num => $is_prime) {
        if ($is_prime) {
                $sum += $num;
        }
}

echo $sum;