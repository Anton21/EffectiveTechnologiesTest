<?php

include_once 'Figure.php';

class Circle implements Figure {
    private $radius;
    
    public function __construct($radius) {
        $this->radius = $radius;
    }
    
    public function calculateSquare(){
        return 3.1415 * $this->radius**2;
    }
}