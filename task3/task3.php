<?php

include_once 'FigureFactory.php';

function fcmp(Figure $figure1, Figure $figure2){
    $square1 = $figure1->calculateSquare();
    $square2 = $figure2->calculateSquare();
    if ($square1 == $square2) return 0;
    return ($square1 > $square2) ? -1 : 1;
}

$str_json = file_get_contents('figures.json');
$json_figures = json_decode($str_json, true);

$figures = [];
foreach ($json_figures as $json_figure) {
    $figures[] = FigureFactory::createFigure($json_figure);
}
   
usort($figures, "fcmp");  
foreach ($figures as $f) {
    print_r($f);
    echo "with square: ".$f->calculateSquare()."\n\n";
}