<?php

interface Figure {
    public function calculateSquare();
}