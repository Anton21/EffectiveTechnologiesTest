<?php

include_once 'Circle.php';
include_once 'Triangle.php';
include_once 'Rectangle.php';

class FigureFactory {
    
    public static function createFigure($json_object) : Figure{
        switch ($json_object['type']){
            case 'circle':
                return new Circle($json_object['radius']);
            case 'rectangle':
                return new Rectangle($json_object['a'], $json_object['b']);
            case 'triangle':
                return new Triangle($json_object['a'], $json_object['b'], $json_object['c']);
            default :
                throw new Exception('Invalid type.');
        }
    } 
}