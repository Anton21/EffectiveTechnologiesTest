<?php

include_once 'Figure.php';

class Triangle implements Figure {
    private $a;
    private $b;
    private $c;
    
    public function __construct($a, $b, $c) {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }
    
    public function calculateSquare(){
        $p = ($this->a + $this->b + $this->c)/2;
        return sqrt($p * ($p - $this->a) * ($p - $this->b) * ($p - $this->c));
    }
}