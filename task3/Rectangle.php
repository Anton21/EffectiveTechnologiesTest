<?php

include_once 'Figure.php';
    
class Rectangle implements Figure {
    private $a;
    private $b;
    
    public function __construct($a, $b) {
        $this->a = $a;
        $this->b = $b;
    }

    public function calculateSquare(){
        return $this->a * $this->b;
    }
}